<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ruta;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\Constraint\SameSize;

class RutaController extends Controller
{
    public function getIndex()
    {
        $rutas = Ruta::all();
        return view('ruta/index', ['rutas' => DB::table('Ruta')->paginate(5)], ['rutas' => $rutas]);
    }

    public function getShow($id)
    {
        $rutas = Ruta::findOrFail($id);
        $user = $rutas->usuario()->first();
        return view('ruta/show', ['ruta' => $rutas, 'user' => $user]);
    }

    public function getCreate()
    {
        return view('ruta/create');
    }

    public function postCreate(Request $request)
    {
        $ruta = new Ruta();
        $user = auth()->user();
        $user->name = auth()->user()->name;
        $user->email = auth()->user()->email;
        $ruta->Usuario_idUsuario = auth()->user()->id;
        $ruta->Origen = $request->input('Origen');
        $ruta->Recorrido = $request->input('Recorrido');
        $ruta->Destino = $request->input('Destino');
        $user->Matricula = $request->input('Matricula');
        $user->Marca = $request->input('Marca');
        $user->Modelo = $request->input('Modelo');
        if (null !== $request->file('imagen')) {
            $user->imagen = asset('storage/' . $request->file('imagen')->hashName());
            $request->file('imagen')->store('public');
        };
        $ruta->save();
        $user->save();
        $request->session()->flash('correcto', 'Se ha creado el registro.');
        return redirect('ruta');
    }

    public function getEdit($id)
    {
        $ruta = Ruta::findOrFail($id);
        $user = $ruta->usuario()->first();
        return view('ruta/edit', ['ruta' => $ruta, 'user' => $user]);
    }

    public function putEdit(Request $request, $id)
    {
        $ruta = Ruta::findOrFail($id);
        $user = $ruta->usuario()->first();
        $ruta->Origen = $request->input('Origen');
        $ruta->Recorrido = $request->input('Recorrido');
        $ruta->Destino = $request->input('Destino');
        $user->Matricula = $request->input('Matricula');
        $user->Marca = $request->input('Marca');
        $user->Modelo = $request->input('Modelo');
        if (null !== $request->file('imagen')) {
            $user->imagen = asset('storage/' . $request->file('imagen')->hashName());
            $request->file('imagen')->store('public');
        };
        $ruta->save();
        $user->save();
        $request->session()->flash('correcto', 'Se ha editado el registro.');
        return redirect('ruta/show/' . $id);
    }

    public function putDelete(Request $request, $id)
    {
        $ruta = Ruta::findOrFail($id);
        $user = $ruta->usuario()->first();
        $ruta->delete();
        $request->session()->flash('correcto', 'Se ha borrado el registro.');
        return redirect('ruta');
    }
}
