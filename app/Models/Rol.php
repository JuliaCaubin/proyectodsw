<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idRol
 * @property string $Rol
 * @property Usuario[] $usuarios
 */
class Rol extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'Rol';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idRol';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['Rol'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usuarios()
    {
        return $this->hasMany('App\Models\User', 'Rol_idRol', 'idRol');
    }
}
