<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idRuta
 * @property string $Origen
 * @property string $Destino
 * @property string $Recorrido
 * @property int $Usuario_idUsuario
 * @property Usuario $usuario
 * @property Usuario[] $usuarios
 */
class Ruta extends Model
{
    use HasFactory;

    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'Ruta';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idRuta';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['Origen', 'Recorrido', 'Destino', 'Usuario_idUsuario'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\User', 'Usuario_idUsuario', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function usuarios()
    {
        return $this->belongsToMany('App\Models\User', 'Ruta_has_Usuario', 'Ruta_idRuta', 'Usuario_idUsuario');
    }
}
