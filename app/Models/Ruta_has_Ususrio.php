<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $Ruta_idRuta
 * @property int $Usuario_idUsuario
 * @property Rutum $rutum
 * @property Usuario $usuario
 */
class Ruta_has_Ususrio extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'Ruta_has_Usuario';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rutum()
    {
        return $this->belongsTo('App\Models\Rutum', 'Ruta_idRuta', 'idRuta');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\User', 'Usuario_idUsuario', 'id');
    }
}
