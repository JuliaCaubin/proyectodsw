<?php

namespace Database\Factories;

use App\Models\Ruta;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class RutaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ruta::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'Origen' => $this->faker->name,
            'Recorrido' => $this->faker->name,
            'Destino' => $this->faker->name,
            'Usuario_idUsuario' => 1
        ];
    }
}
