<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRutaHasUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Ruta_has_Usuario', function (Blueprint $table) {
            $table->integer('Ruta_idRuta')->index('fk_Ruta_has_Usuario_Ruta_idx')->unsigned();
            $table->integer('Usuario_idUsuario')->index('fk_Ruta_has_Usuario_Usuario1_idx')->unsigned();
            $table->primary(['Ruta_idRuta', 'Usuario_idUsuario']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Ruta_has_Usuario');
    }
}
