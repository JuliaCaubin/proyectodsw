<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRutaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Ruta', function (Blueprint $table) {
            $table->increments('idRuta');
            $table->string('Origen', 45);
            $table->string('Recorrido', 45);
            $table->string('Destino', 45);
            $table->integer('Usuario_idUsuario')->index('fk_Ruta_Usuario1_idx')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Ruta');
    }
}
