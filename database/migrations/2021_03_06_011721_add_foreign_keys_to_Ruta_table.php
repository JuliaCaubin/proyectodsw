<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToRutaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Ruta', function (Blueprint $table) {
            $table->foreign('Usuario_idUsuario', 'fk_Ruta_Usuario1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Ruta', function (Blueprint $table) {
            $table->dropForeign('fk_Ruta_Usuario1');
        });
    }
}
