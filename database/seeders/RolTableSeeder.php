<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Rol;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RolTableSeeder extends Seeder
{
    /*private $arrayRol = array(
        array(
            'Rol' => 'Administrador'
        ),
        array(
            'Rol' => 'Conductor'
        ),
        array(
            'Rol' => 'Pasajero'
        )
    );*/
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Rol')->delete();
        DB::table('Rol')->insert(array (
            0 => 
            array (
                'Rol' => 'Administrador'
            ),
            1 => 
            array (
                'Rol' => 'Conductor'
            ),
            2 => 
            array (
                'Rol' => 'Pasajero'
            ),
        ));

        /*foreach ($this->arrayRol as $rol) {
            $r = new Rol;
            $r->Rol = $rol['Rol'];
            $r->save();
        }*/
    }
}
