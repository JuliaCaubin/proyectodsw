<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Ruta;
use Illuminate\Support\Facades\DB;

class RutaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Ruta')->delete();
        Ruta::factory()->count(13)->create();
    }
}
