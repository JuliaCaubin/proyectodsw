<div class="wrapper row4">
    <footer id="footer" class="hoc clear">
        <div class="one_half first">
            <h6 class="heading">Contacto</h6>
            <ul class="nospace linklist contact">
                <li><i class="fa fa-map-marker"></i>
                    <address>
                        Majada Marcial
                    </address>
                </li>
                <li><i class="fa fa-phone"></i> 666136665</li>
                <li><i class="fa fa-envelope-o"></i> admin@gmail.com</li>
            </ul>
        </div>

        <div class="one_half">
            <h6 class="heading">Licencia</h6>
            <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.
        </div>
    </footer>
</div>
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>