<div class="bgded overlay" ">
    <div class=" wrapper row1">
    <header id="header" class="hoc clear">
        <div id="logo" class="fl_left">
            <h1><a href="{{url('/ruta')}}">ShareCar</a></h1>
        </div>
        @if(Auth::check() )
        <nav id="mainav" class="fl_right">
            <ul class="clear">
                <li class="nav-item {{ Request::is('ruta') && ! Request::is('ruta/create') ? 'active' : ''}}"><a href="{{url('/ruta')}}">Rutas</a></li>
                <li class="nav-item {{ Request::is('ruta/create') ? 'active' : ''}}"><a href="{{url('/ruta/create')}}">Nueva ruta</a></li>
                <form action="{{ url('/logout') }}" method="POST" style="display:inline">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-link nav-link" style="display:inline;cursor:pointer">
                        <i class="fa fa-sign-out" aria-hidden="true"></i> Cerrar sesión
                    </button>
                </form>
            </ul>
        </nav>
        @endif
    </header>
</div>