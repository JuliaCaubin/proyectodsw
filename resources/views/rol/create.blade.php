@extends('layouts.master')

@section('content')

<div class="row" style="margin-top:40px">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">
                    Añadir ruta
                </div>
                <div class="card-body" style="padding:30px">
                    <form action="" method="post" enctype="multipart/form-data">
                        @CSRF
                        <div class="form-group">
                            <label for="origen">Origen</label>
                            <input type="text" name="origen" id="origen" class="form-control" value="{{old('origen')}}">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="recorrido">Recorrido</label>
                            <input type="text" name="recorrido" id="recorrido" value="{{old('recorrido')}}">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="destino">Destino</label>
                            <input type="text" name="destino" id="destino" value="{{old('destino')}}">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="imagen">Selecciona una imagen</label>
                            <input type="file" name="imagen" id="imagen" value="{{old('imagen')}}">
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                                Añadir cliente
                            </button>
                        </div>
                    </form>
                    @if(Session::has('correcto'))
                    <div class="alert alert-success"> {{ Session::get('correcto') }}</div>
                    @endif

                    @if ($errors->any())
                    <div class="row justify-content-center">
                        <div class="col-sm-7">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop