@extends('layouts.master')

@section('content')

<div class="wrapper">
    <div id="pageintro" class="hoc clear" style="height: 100px;">
        <article>
            <div>
                <h2 class="heading">ShareCar</h2>
                <p>Pagina para compartir coche en Fuerteventura.</p>
            </div>
        </article>
    </div>
</div>
</div>
<!-- End Top Background Image Wrapper -->
<div class="wrapper row3">
    <main class="hoc container clear">
        <!-- main body -->
        <div class="sectiontitle">
            <h6 class="heading">Buscador de rutas</h6>
        </div>
        <div class="group">
            <form action="#" method="post">
                <div class="one_half first">
                    <label for="origen">Origen</label>
                    <input type="text" name="origen" id="origen" value="" size="33" style="border-radius: 5px;">
                </div>
                <div class="one_half">
                    <label for="destino">Destino</label>
                    <input type="text" name="destino" id="destino" value="" size="33" style="border-radius: 5px;">
                </div>
            </form>
            <p class="btmspace-50"> Tabla resultados</p>
        </div>
        <!-- / main body -->
        <div class="clear"></div>
    </main>
</div>
<div class="wrapper row3">
    <div class="hoc container clear">
        <div class="sectiontitle">
            <h6 class="heading">Todas las rutas</h6>
            <p>Tablas en la que se muestran todas las tutas creadas</p>
        </div>
        <ul class="nospace group services">
            <table>
                <th>Origen</th>
                <th>Recorrido</th>
                <th>Destino</th>
                @foreach($rutas as $ruta)
                <tr>
                    <a href="{{ url('/ruta/show/' . $ruta->id) }}">
                        <td>{{$ruta->origen}}</td>
                        <td>{{$ruta->recorrido}}</td>
                        <td>{{$ruta->destino}}</td>
                    </a>
                    <a href="{{ url('/ruta/show/' . $ruta->id) }}" class="btn btn-info" role="button" style="background-color: aquamarine; color: black;">
                        Ver ruta</a>
                </tr>
                @endforeach
            </table>
        </ul>
    </div>
</div>

@stop