@extends('layouts.master')

@section('content')

<div class="wrapper row3">
    <main class="hoc container clear">
        <!-- main body -->
        <div class="sectiontitle">
            <h6 class="heading">Crear ruta</h6>
        </div>
        <div class="group">
            <form action="" method="post" enctype="multipart/form-data">
                @CSRF
                <div class="form-group">
                    <label for="Origen">Origen</label>
                    <input type="text" name="Origen" id="Origen" class="form-control">
                </div>
                <br>
                <div class="form-group">
                    <label for="Recorrido">Recorrido</label>
                    <input type="text" name="Recorrido" id="Recorrido" }}">
                </div>
                <br>
                <div class="form-group">
                    <label for="Destino">Destino</label>
                    <input type="text" name="Destino" id="Destino"">
                </div>
                <br>
                <div class=" form-group">
                    <label for="Matricula">Matricula</label>
                    <input type="text" name="Matricula" id="Matricula" }}">
                </div>
                <br>
                <div class="form-group">
                    <label for="Marca">Marca</label>
                    <input type="text" name="Marca" id="Marca" </div>
                    <br>
                    <div class="form-group">
                        <label for="Modelo">Modelo</label>
                        <input type="text" name="Modelo" id="Modelo">
                    </div>
                    <br>
                    <div class="form-group">
                        <label for="imagen">Selecciona una imagen</label>
                        <input type="file" name="imagen" id="imagen">
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                            Añadir ruta
                        </button>
                    </div>
            </form>
            @if(Session::has('correcto'))
            <div class="alert alert-success"> {{ Session::get('correcto') }}</div>
            @endif

            @if ($errors->any())
            <div class="row justify-content-center">
                <div class="col-sm-7">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </main>
</div>
@stop