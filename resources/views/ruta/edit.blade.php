@extends('layouts.master')

@section('content')

<div class="wrapper row3">
    <main class="hoc container clear">
        <!-- main body -->
        <div class="sectiontitle">
            <h6 class="heading">Modificar ruta</h6>
        </div>
        <div class="group">
            <form action="" method="post" enctype="multipart/form-data">
                @method('PUT')
                @CSRF
                <h2><b>Nombre conductor: </b>{{$user->name}}</h2>
                <br>
                <h3><b>Correo-e: </b>{{$user->email}}</h3>
                <div class="form-group">
                    <label for="Origen">Origen</label>
                    <input type="text" name="Origen" id="Origen" class="form-control" value="{{$ruta->Origen}}">
                </div>
                <br>
                <div class="form-group">
                    <label for="Recorrido">Recorrido</label>
                    <input type="text" name="Recorrido" id="Recorrido" value="{{$ruta->Recorrido}}">
                </div>
                <br>
                <div class="form-group">
                    <label for="Destino">Destino</label>
                    <input type="text" name="Destino" id="Destino" value="{{$ruta->Destino}}">
                </div>
                <br>
                <div class="form-group">
                    <label for="Matricula">Matricula</label>
                    <input type="text" name="Matricula" id="Matricula" value="{{$user->Matricula}}">
                </div>
                <br>
                <div class="form-group">
                    <label for="Marca">Marca</label>
                    <input type="text" name="Marca" id="Marca" value="{{$user->Marca}}">
                </div>
                <br>
                <div class="form-group">
                    <label for="Modelo">Modelo</label>
                    <input type="text" name="Modelo" id="Modelo" value="{{$user->Modelo}}">
                </div>
                <br>
                <div class="form-group">
                    <label for="imagen">Selecciona una imagen</label>
                    <input type="file" name="imagen" id="imagen" value="{{$user->imagen}}">
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                        Modificar ruta
                    </button>
                </div>
            </form>
            @if(Session::has('correcto'))
            <div class="alert alert-success"> {{ Session::get('correcto') }}</div>
            @endif

            @if ($errors->any())
            <div class="row justify-content-center">
                <div class="col-sm-7">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </main>
</div>
@stop