@extends('layouts.master')

@section('content')

<div class="wrapper">
    <div id="pageintro" class="hoc clear" style="height: 100px;">
        <article>
            <div>
                <h2 class="heading">ShareCar</h2>
                <p>Pagina para compartir coche en Fuerteventura.</p>
            </div>
        </article>
    </div>
</div>
</div>
<!-- End Top Background Image Wrapper -->
<div class="wrapper row3">
    <main class="hoc container clear">
        <!-- main body -->
        <div class="sectiontitle">
            <h6 class="heading">Buscador de rutas</h6>
        </div>
        <table class="buscar">
            <thead>
                <tr>
                    <th>Origen</th>
                    <th>Recorrido</th>
                    <th>Destino</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($rutas as $ruta)
                <tr>
                    <td>{{$ruta->Origen}}</td>
                    <td>{{$ruta->Recorrido}}</td>
                    <td>{{$ruta->Destino}}</td>
                    <td><a href="{{ url('/ruta/show/' . $ruta->idRuta) }}" class="btn btn-info" role="button" style="background-color: aquamarine; color: black;">
                            Ver ruta</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
</div>
<!-- / main body -->
<div class="clear"></div>
</main>
</div>
<div class="wrapper row3">
    <div class="hoc container clear">
        <div class="sectiontitle">
            <h6 class="heading">Todas las rutas</h6>
            <p>Tablas en la que se muestran todas las tutas creadas</p>
        </div>
        <ul class="nospace group services">
            <table>
                <th>Origen</th>
                <th>Recorrido</th>
                <th>Destino</th>
                <th>Action</th>
                @foreach($rutas as $ruta)
                <tr>
                    <td>{{$ruta->Origen}}</td>
                    <td>{{$ruta->Recorrido}}</td>
                    <td>{{$ruta->Destino}}</td>
                    <td><a href="{{ url('/ruta/show/' . $ruta->idRuta) }}" class="btn btn-info" role="button" style="background-color: aquamarine; color: black;">
                            Ver ruta</a></td>
                </tr>
                @endforeach
            </table>
        </ul>
        {{$ruta = DB::table('Ruta')->simplePaginate(5)}}
    </div>
</div>
@stop