@extends('layouts.master')
@include('ruta.destroy')
@section('content')

<div class="wrapper row3">
  <main class="hoc container clear">
    <!-- main body -->
    <div class="sectiontitle">
      <h6 class="heading">Ruta y datos usuario</h6>
    </div>
    <div class="group">
      <div class="one_half first">
        <h2><b>Nombre conductor: </b>{{$user->name}}</h2>
        <br>
        <h3><b>Correo-e: </b>{{$user->email}}</h3>
        <br>
        <h3><b>Origen: </b>{{$ruta->Origen}}</h3>
        <h3><b>Recorrido: </b>{{$ruta->Recorrido}}</h3>
        <h3><b>Destino: </b>{{$ruta->Destino}}</h3>
        <br>
        <h3><b>Matricula: </b>{{$user->Matricula}}</h3>
        <br>
        <h3><b>Marca: </b>{{$user->Marca}}</h3>
        <h3><b>Modelo: </b>{{$user->Modelo}}</h3>
      </div>
      <div class="one_half">
        @if($user->imagen !== null)
        <div class="one_half"><img class="inspace-10 borderedbox" style="height:300px" src="{{$user->imagen}}" alt=""></div>
        @endif
      </div>
      <br>
      <a href="{{url('/ruta/edit/' . $ruta->idRuta)}}" class="btn btn-info" role="button" style="background-color: yellow; color: black;">Editar</a>
      <a href="/ruta" class="btn btn-info" role="button" style="background-color: aquamarine; color: black;">
        < Volver</a>
          <button type="button" class="btn btn-danger ml-2" data-toggle="modal" data-target="#modal-delete-{{$ruta->idRuta}}">
            Eliminar
          </button>
    </div>
    @if(Session::has('correcto'))
    <div class="alert alert-success"> {{ Session::get('correcto') }}</div>
    @endif
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>

@stop