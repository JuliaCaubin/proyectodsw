<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RutaController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => 'true']);

Route::group(['middleware' => 'verified'], function () {

    Route::get('/', [HomeController::class, 'getHome'])->middleware(['password.confirm']);

    Route::get('/ruta', [RutaController::class, 'getIndex'])->middleware(['password.confirm']);

    Route::get('/ruta/show/{id}', [RutaController::class, 'getShow'])->middleware(['password.confirm']);

    Route::get('/ruta/create', [RutaController::class, 'getCreate']);
    Route::post('/ruta/create', [RutaController::class, 'postCreate'])->middleware(['password.confirm']);

    Route::get('/ruta/edit/{id}', [RutaController::class, 'getEdit']);
    Route::put('/ruta/edit/{id}', [RutaController::class, 'putEdit'])->middleware(['password.confirm']);

    Route::delete('/ruta/delete/{id}', [RutaController::class, 'putDelete'])->middleware(['password.confirm']);;

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});

